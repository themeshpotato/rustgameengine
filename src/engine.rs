extern crate sdl2;

use renderer::*;
use std::time::{SystemTime};

pub enum Key {
    Up = 0,
    Down = 1,
    Left = 2,
    Right = 3,
    Escape = 4,
    Max = 5
}

pub enum GraphicsApi {
    OpenGL,
    DirectX,
    Vulkan,
    Metal
}

pub struct InputState {
    event_pump: sdl2::EventPump,
    keys_down: [bool; Key::Max as usize],
    should_close: bool
}

impl InputState {
    fn update_events(&mut self) {
        for event in self.event_pump.poll_iter() {
            match event {
                sdl2::event::Event::Quit {..} => { self.should_close = true },
                sdl2::event::Event::KeyDown { keycode, .. } => {
                    if let Some(code) = keycode {
                        match code {
                            sdl2::keyboard::Keycode::Escape => {
                                self.keys_down[Key::Escape as usize] = true;
                            },
                            sdl2::keyboard::Keycode::Up => {
                                self.keys_down[Key::Up as usize] = true;
                            },
                            sdl2::keyboard::Keycode::Down => {
                                self.keys_down[Key::Down as usize] = true;
                            },
                            sdl2::keyboard::Keycode::Left => {
                                self.keys_down[Key::Left as usize] = true;
                            },
                            sdl2::keyboard::Keycode::Right => {
                                self.keys_down[Key::Right as usize] = true;
                            },
                            _ => {}
                        }
                    }
                }

                sdl2::event::Event::KeyUp { keycode, .. } => {
                    if let Some(code) = keycode {
                        match code {
                            sdl2::keyboard::Keycode::Up => {
                                self.keys_down[Key::Up as usize] = false;
                            },
                            sdl2::keyboard::Keycode::Down => {
                                self.keys_down[Key::Down as usize] = false;
                            },
                            sdl2::keyboard::Keycode::Left => {
                                self.keys_down[Key::Left as usize] = false;
                            },
                            sdl2::keyboard::Keycode::Right => {
                                self.keys_down[Key::Right as usize] = false;
                            },
                            _ => {}
                        }
                    }
                },
                _ => {},
            }
        }
    }

    pub fn key_down(&self, keycode: Key) -> bool {
        self.keys_down[keycode as usize]
    }
}

pub struct Engine {
    _graphics_api: GraphicsApi,
    pub renderer: Renderer,
    pub input_state: InputState
}

impl Engine {
    pub fn init(graphics_api: GraphicsApi) -> Result<Engine, String> {
        // At the moment only OpenGL is supported
        match graphics_api {
            GraphicsApi::OpenGL => {
                // Initialize sdl2
                let sdl = sdl2::init().unwrap();
                let video_subsystem = sdl.video().unwrap();

                // Get the event pump
                let mut event_pump = sdl.event_pump().unwrap();
                event_pump.enable_event(sdl2::event::EventType::KeyDown);

                // Initialize the Renderer
                let renderer = Renderer::create(video_subsystem, 1280, 720);

                return Ok(Engine {
                    _graphics_api: graphics_api,
                    renderer: renderer,
                    input_state: InputState { event_pump: event_pump, keys_down: [false; Key::Max as usize], should_close: false }
                });
            },
            _ => {
                Err(String::from("Unsupported graphics API. Only OpenGL is currently supported"))
            }
        }
    }

    pub fn run<F>(&mut self, update_function: &mut F) where F: FnMut(f64, &mut Engine) {
        let mut current_time = SystemTime::now();

        'main: loop {
            let start = SystemTime::now();
            let delta = start.duration_since(current_time).unwrap();
            current_time = start;
            let dt = delta.as_secs() as f64 + delta.subsec_nanos() as f64 * 1e-9;

            // Update the game
            self.input_state.update_events();
            update_function(dt, self);

            if self.input_state.should_close {
                break 'main;
            }

            // Update animations and render everything
            self.renderer.update_animations(dt);
            self.renderer.render();
        }
    }

    pub fn exit(&mut self) {
        self.input_state.should_close = true;
    }
}
